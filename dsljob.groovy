job('dsl challenge'){
    parameters {
        stringParam("archivohtml", 'challenge3cvar.html' , 'archivo a copiar')
        stringParam("hosts", '/home/sdelpino/Escritorio/Ansible/hoststest' , 'archivo hosts')
        stringParam("hola","afa","afa")
    }
    
    scm{
        
        git("https://gitlab.com/sgdelpino/jenkinsansible.git", 'master')
    }
    
    triggers {
        scm 'H/5 * * * *'
    }

    steps {
        shell 'ansible-playbook -i ${hosts} playbookfilecopy3cvar.yml -e "archivohtml=$archivohtml servicio=httpd varrestart=false"'  
    }
}